package com.google.ar.sceneform.samples.gltf;

import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.google.ar.core.Anchor;
import com.google.ar.core.Camera;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.Scene;
import com.google.ar.sceneform.collision.Ray;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.math.Vector3Evaluator;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.Texture;
import com.google.ar.sceneform.ux.BaseArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

public class ARActivity extends BaseARActivity implements BaseArFragment.OnTapArPlaneListener, Scene.OnUpdateListener {

    private Node sphereNode = null;     // sphere that is being shot on tigers. Can be multiples in scene
    private Point point;                // point is set as screen size to do /2 operation to find a center of screen for shooting line
    private TransformableNode[] tigersArr = new TransformableNode[5]; // array of 5 tigers, null indexes at start
    private int tigersArrIndex = 0;
    private boolean allTigersSet = false;  // When player creates all 5 tigers, is set to true
    private int tigersLeft = 0;
    private boolean gameFinished = false;  // When last tiger is killed, this is set to true
    private boolean shouldStartTimer = true;    // Timer starts counting down to 0 from the first tiger placement


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // determine phone screen size to use it later as center
        Display display = getWindowManager().getDefaultDisplay();
        point = new Point();
        display.getRealSize(point);

        // display only --Place tigers text-- for player
        Button shootButton = findViewById(R.id.shootButton);
        shootButton.setVisibility(View.GONE);
        Button restartButton = findViewById(R.id.restartButton);
        restartButton.setVisibility(View.GONE);
        TextView winText = findViewById(R.id.gameResultText);
        winText.setVisibility(View.GONE);

        // detekce tapnutí na plochu
        arFragment.setOnTapArPlaneListener(this);
        // detekce změny pozice zařízení
        arFragment.getArSceneView().getScene().addOnUpdateListener(this);
    }


    @Override
    public void onTapPlane(HitResult hitResult, Plane plane, MotionEvent motionEvent) {
        if (renderable == null) {
            return;
        }
        // add tigers to scene if still less than 5 and shooting didn't start yet. After 5 tigers you will never create more
        if (!allTigersSet && tigersLeft < 5) {
            createTiger(hitResult.createAnchor());
            tigersLeft++;

            //start timer on first interaction
            if (shouldStartTimer) {
                startTimer();
                shouldStartTimer = false;
            }
            // on all tigers, play UI gets displayed - shoot button, tigers help dissapears
            if (tigersLeft == 5) {
                allTigersSet = true;
                tigersArrIndex = 4;
                TextView tigersHelptext = findViewById(R.id.placeTigersInfo);
                tigersHelptext.setVisibility(View.GONE);
                Button shootButton = findViewById(R.id.shootButton);
                shootButton.setVisibility(View.VISIBLE);
            }
        }
    }

    private void createTiger(Anchor anchor) {
        Anchor tigerAnchor = anchor;
        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setParent(arFragment.getArSceneView().getScene());

        TransformableNode tigerNode = new TransformableNode(arFragment.getTransformationSystem());
        tigerNode.setEnabled(true);
        tigerNode.setRenderable(renderable);
        tigerNode.setWorldScale(new Vector3(0.1f, 0.1f, 0.1f));
        tigerNode.setParent(anchorNode);
        tigerNode.select();

        tigersArr[tigersArrIndex] = tigerNode;
        tigersArrIndex++;
    }

    // metoda volána při změně pozice zařízení. Takže v podstatě pořád.
    @Override
    public void onUpdate(FrameTime frameTime) {

        Frame frame = arFragment.getArSceneView().getArFrame();
        Camera camera = frame.getCamera();
        // ukázka použití vzdálenosti.
        if (camera.getTrackingState() == TrackingState.TRACKING) {
            // Keep all tigers to watch player
            rotateTigersToCamera();
        }

    }

    public void rotateTigersToCamera() {
        Vector3 cameraPosition = getCameraPosition();
        for (TransformableNode tigerNode : tigersArr) {
            if (tigerNode != null) {
                Vector3 tigerPosition = tigerNode.getWorldPosition();
                cameraPosition.set(cameraPosition.x, tigerPosition.y, cameraPosition.z);
                Vector3 direction = Vector3.subtract(tigerPosition, cameraPosition);
                Quaternion lookRotation = Quaternion.lookRotation(direction, Vector3.up());
                tigerNode.setWorldRotation(lookRotation);
            } else {
                break;
            }
        }
    }

    private void startTimer() {
        TextView timer = findViewById(R.id.timerText);

        new Thread(() -> {
            int seconds = 30;

            while (tigersLeft > 0 && seconds > 0) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                seconds--;

                // Set text on UI thread, We cannot update UI from background thread
                int finalSeconds = seconds;
                runOnUiThread(() -> timer.setText(Integer.toString(finalSeconds)));

            }

            int finalSeconds1 = seconds;
            runOnUiThread(() -> resolveGameEnd(tigersLeft, finalSeconds1));

        }).start();

    }

    // create texture and size of sphere that is being shot later
    private ModelRenderable buildSphereModel() {
        Color renderColor = new Color((float)(Math.random()),(float)(Math.random()),(float)(Math.random()));
        final ModelRenderable[] sphereGeneratingModel = new ModelRenderable[1];    // Just a texture of sphere
        MaterialFactory.makeTransparentWithColor(getApplicationContext(), renderColor)
                .thenAccept(
                        material -> {
                            // volání je asynchronní, proto je potřeba nejdřív počkat na vytvoření.
                            sphereGeneratingModel[0] = ShapeFactory.makeSphere(0.05f,
                                    new Vector3(0f, 0f, 0f), material);
                        });
        return sphereGeneratingModel[0];
    }

    /**
     * Voláno při zmáčknutí tlačítka
     *
     * @param view
     */
    public void shoot(View view) {
        Scene scene = arFragment.getArSceneView().getScene();
        Frame frame = arFragment.getArSceneView().getArFrame();
        com.google.ar.sceneform.Camera camera = scene.getCamera();
        ModelRenderable sphereModel = buildSphereModel();

        // creating line in center of camera sight
        Ray ray = camera.screenPointToRay(point.x / 2f, point.y / 2f);
        sphereNode = new Node();
        sphereNode.setRenderable(sphereModel);
        scene.addChild(sphereNode);

        final boolean[] alreadyFoundContactNode = {false};
        // sending sphere by the line away from user until it collides eventually
        new Thread(() -> {
            for (int i = 0; i < 700; i++) {
                int finalI = i;
                runOnUiThread(() -> {
                    Vector3 vector3 = ray.getPoint(finalI * 0.01f);
                    sphereNode.setWorldPosition(vector3);

                    Node nodeInContact = scene.overlapTest(sphereNode);
                    try {
                        if (nodeInContact != null) {
                            float distance = getDistanceToObject(getObjectPositionWhenShot(nodeInContact), getCameraPositionWhenShot());
                            // remove tiger only if max 2 meters away
                            if (distance > 2) {
                                // this if exists because the sphere was removing more tigers by one shot (multicollision) because of speed and frames
                                if (!alreadyFoundContactNode[0]) {
                                    alreadyFoundContactNode[0] = true;
                                    scene.removeChild(nodeInContact.getParent());
                                    tigersLeft--;
                                    scene.removeChild(sphereNode);
                                }
                            }
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                });
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // Remove shot sphere from the scene after travelled distance
            try {
                runOnUiThread(() -> scene.removeChild(sphereNode));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }


    private void resolveGameEnd(int tigersLeft, int seconds) {
        Button shootButton = findViewById(R.id.shootButton);
        shootButton.setVisibility(View.GONE);

        TextView resultText = findViewById(R.id.gameResultText);
        Button restartButton = findViewById(R.id.restartButton);
        if (tigersLeft == 0 && seconds > 0) {
            resultText.setText("You win!");
        }
        if (tigersLeft > 0 && seconds == 0) {
            resultText.setText("You lost!");
        }
        if (tigersLeft == 0 && seconds == 0) {
            resultText.setText("You win!");
        }

        resultText.setVisibility(View.VISIBLE);
        restartButton.setVisibility(View.VISIBLE);
    }

    private Vector3 getCameraPosition() {
        Frame frame = arFragment.getArSceneView().getArFrame();
        Camera camera = frame.getCamera();
        Pose cameraPose = camera.getDisplayOrientedPose();
        return new Vector3(cameraPose.tx(), cameraPose.ty(), cameraPose.tz());
    }


    private Pose getObjectPositionWhenShot(Node objectNode) {
        Frame frame = arFragment.getArSceneView().getArFrame();
        float[] translation = new float[3];
        translation[0] = (float) objectNode.getWorldPosition().x;
        translation[1] = (float) objectNode.getWorldPosition().y;
        translation[2] = (float) objectNode.getWorldPosition().z;
        float[] rotation = {0.0f, 0.0f, 0.0f, 0.0f};

        return new Pose(translation, rotation);
    }

    private Pose getCameraPositionWhenShot() {
        Frame frame = arFragment.getArSceneView().getArFrame();
        Camera camera = frame.getCamera();
        Pose cameraPose = camera.getDisplayOrientedPose();
        return cameraPose;
    }

    private float getDistanceToObject(Pose objectPose, Pose cameraPose) {
        float distanceX = objectPose.tx() - cameraPose.tx();
        float distanceY = objectPose.ty() - cameraPose.ty();
        float distanceZ = objectPose.tz() - cameraPose.tz();
        return (float) Math.sqrt(distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ) - 0.5f;
    }

    /**
     * Voláno při zmáčknutí tlačítka Restart
     *
     * @param view
     */
    public void Restart(View view) {
        this.recreate();
    }


    // unused
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
